package test.com.echoing.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import junit.framework.TestCase;

import org.elasticsearch.index.query.QueryBuilders;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;

import test.com.echoing.AbstractTest;
import test.com.echoing.util.MockFactory;

import com.echoing.elasticsearch.service.ElasticSearchService;
import com.echoing.elasticsearch.util.ElasticSearchEntityType;
import com.echoing.elasticsearch.util.LocalizedMessageParser;
import com.echoing.model.Coordinates;
import com.echoing.model.User;
import com.echoing.model.UserPosition;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RestEndpointApiTest extends AbstractTest{
	
	
	@Autowired
	private ElasticSearchService elasticSearchService;

	@Autowired
	@Qualifier("localizedMessageParser")
	private LocalizedMessageParser localizedMessageParser;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Test
	public void pushMessagesTest() throws Exception{
		restEndpoint.perform(post("/Message").contentType(MediaType.APPLICATION_JSON_VALUE).content(localizedMessageParser.parse(MockFactory.getMockMessage()).string())).andExpect(status().isOk());
	}
	
	@Test
	public void pullMessages() throws Exception{
		
		UserPosition userPosition = new UserPosition();
		User user = new User();
		user.setUserId("1L");
		user.setEmail("test@test.com");
		userPosition.setUser(user);
		Coordinates coordinates = new Coordinates();
		coordinates.setLatitude(10.0);
		coordinates.setLongitude(10.0);
		userPosition.setCoordinates(coordinates);
		
		restEndpoint.perform(post("/PositionedMessages").contentType(MediaType.APPLICATION_JSON_VALUE).content(mapper.writeValueAsString(userPosition))).andExpect(status().isOk());
		
		TestCase.assertEquals(1L, esClient.prepareSearch("test").setTypes(ElasticSearchEntityType.LOCALIZED_MESSAGE.getType()).setQuery(QueryBuilders.matchAllQuery()).execute().actionGet().getHits().getTotalHits());
		
	}

}
