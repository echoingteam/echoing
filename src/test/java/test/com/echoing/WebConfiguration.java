package test.com.echoing;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@EnableWebMvc
@ComponentScan(basePackageClasses={com.echoing.rest.RestEndpointApiImpl.class})
public class WebConfiguration {
	
	@Bean
	public ObjectMapper getObjectMapper(){
		return new ObjectMapper();
	}
	
	@Bean
	public MappingJackson2HttpMessageConverter getMessageConverter(){
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setObjectMapper(getObjectMapper());
		return converter;
	}
}
