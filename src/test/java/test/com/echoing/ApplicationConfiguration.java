package test.com.echoing;

import org.elasticsearch.client.Client;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.echoing.elasticsearch.service.ElasticSearchService;
import com.echoing.elasticsearch.service.ElasticSearchServiceImpl;
import com.echoing.elasticsearch.util.EntityParser;
import com.echoing.elasticsearch.util.LocalizedMessageParser;
import com.echoing.elasticsearch.util.MappingHelper;
import com.echoing.elasticsearch.util.MappingHelperImpl;
import com.echoing.elasticsearch.util.UserParser;
import com.echoing.log.EchoingLogger;
import com.echoing.log.EchoingLoggerImpl;
import com.echoing.model.LocalizedMessage;
import com.echoing.model.User;


@Configuration
public class ApplicationConfiguration {
	
	@Bean
	public ElasticSearchService getElasticSearchService() throws Exception{
		ElasticSearchServiceImpl elasticSearchService = new ElasticSearchServiceImpl();
		elasticSearchService.setElasticsearchClient(getClient());
		elasticSearchService.setMappingHelper(getMappingHelper());
		elasticSearchService.setSearchMaxTime(2);
		elasticSearchService.setSearchRadio(1D);
		return elasticSearchService;
	}
	
	@Bean
	public Node getNode(){
		return NodeBuilder.nodeBuilder().local(true).node();
	}
	
	@Bean
	public Client getClient(){
		return getNode().client();
	}
	
	@Bean
	public MappingHelper getMappingHelper() throws Exception{
		MappingHelperImpl mappingHelper =new MappingHelperImpl();
		mappingHelper.setIndexAlias(AbstractTest.TEST_INDEX_NAME);
		return mappingHelper;
	}
	
	@Bean
	@Qualifier("localizedMessageParser")
	public EntityParser<LocalizedMessage> getLocalizedMessageParser(){
		return new LocalizedMessageParser();
	}
	
	@Bean
	@Qualifier("userParser")
	public EntityParser<User> getUserParser(){
		return new UserParser();
	}
	
	@Bean
	public EchoingLogger getEchoingLogger(){
		return new EchoingLoggerImpl() {
			
			@Override
			public void log(LocalizedMessage myMessage) {
				System.out.println("Log message test:" + myMessage);
			}
		};
	}
}
