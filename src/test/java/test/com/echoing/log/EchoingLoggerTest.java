package test.com.echoing.log;

import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;

import test.com.echoing.util.MockFactory;

import com.echoing.log.EchoingLoggerImpl;
import com.echoing.model.LocalizedMessage;

/**
 * 
 * @author tglman
 * 
 */
public class EchoingLoggerTest {

	@Test
	public void logTest() {
		EchoingLoggerImpl myLogger = new EchoingLoggerImpl();

		Logger mockLogger = Mockito.mock(Logger.class);
		myLogger.setLogger(mockLogger);

		LocalizedMessage myMessage = MockFactory.getMockMessage();
		myLogger.log(myMessage);

		Mockito.verify(mockLogger, Mockito.atLeastOnce()).info(
				"Jorge I love you |" + myMessage.getMessageContent() + "|"
						+ myMessage.getUuid() + "|" + myMessage.getTimestamp().getMillis());
	}

	@Test
	public void realLogTest() {
		EchoingLoggerImpl myLogger = new EchoingLoggerImpl();

		LocalizedMessage myMessage = MockFactory.getMockMessage();
		myLogger.log(myMessage);

	}

}
