package test.com.echoing;

import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.node.Node;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.echoing.elasticsearch.service.ElasticSearchService;
import com.echoing.elasticsearch.util.ElasticSearchEntityType;
import com.echoing.elasticsearch.util.LocalizedMessageParser;
import com.echoing.elasticsearch.util.MappingHelper;
import com.echoing.elasticsearch.util.UserParser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy({
    @ContextConfiguration(classes = ApplicationConfiguration.class),
    @ContextConfiguration(classes = WebConfiguration.class)
})
@WebAppConfiguration
public abstract class AbstractTest {
	/**
	 * The test index name
	 */
	public static final String TEST_INDEX_NAME = "test";
	/**
	 * The web app context
	 */
	@Autowired
	private WebApplicationContext wac;
	/**
	 * The mock MVC
	 */
	protected MockMvc restEndpoint;
	
	@Autowired
	protected ElasticSearchService elasticSearchService;
	
	@Autowired
	protected MappingHelper mappingHelper;
	
	@Autowired
	protected Client esClient;
	
	@Autowired
	private Node node;
	
	@Autowired
	private Client client;
	
	@Autowired
	@Qualifier("localizedMessageParser")
	private LocalizedMessageParser localizedMessageParser;
	
	@Autowired
	@Qualifier("userParser")
	private UserParser userParser;
	
	@Before
	public void init() throws Exception{
		restEndpoint = MockMvcBuilders.webAppContextSetup(this.wac).build();
		deleteTestIndex();
		esClient.admin().indices().create(new CreateIndexRequest(TEST_INDEX_NAME)).actionGet();		
		esClient.admin().indices().preparePutMapping(TEST_INDEX_NAME).setType(ElasticSearchEntityType.USER.getType()).setSource(userParser.getMapping()).execute().actionGet();
		esClient.admin().indices().preparePutMapping(TEST_INDEX_NAME).setType(ElasticSearchEntityType.LOCALIZED_MESSAGE.getType()).setSource(localizedMessageParser.getMapping()).execute().actionGet();
	}
	
	@After
	public void shutdown(){
		deleteTestIndex();
		node.stop();
	}
	/**
	 * Deletes the test index 
	 */
	private void deleteTestIndex(){
		if(esClient.admin().indices().prepareExists(TEST_INDEX_NAME).execute().actionGet().isExists()){
			DeleteIndexRequest deleteRequest = new DeleteIndexRequest(TEST_INDEX_NAME);
			esClient.admin().indices().delete(deleteRequest);
		}		
	}

	/**
	 * @param wac the wac to set
	 */
	public void setWac(WebApplicationContext wac) {
		this.wac = wac;
	}
}
