package test.com.echoing.util;

import com.echoing.model.Coordinates;
import com.echoing.model.LocalizedMessage;
import com.echoing.model.User;

public class MockFactory {
	
	public static LocalizedMessage getMockMessage(){
		LocalizedMessage message = new LocalizedMessage();
		message.setCoordinates(new Coordinates());
		message.getCoordinates().setLatitude(10.0);
		message.getCoordinates().setLongitude(10.0);
		message.setMessageContent("TEST");
		message.setUuid("UUID");
		message.getUser().setEmail("test@test.com");
		return message;
	}
}
