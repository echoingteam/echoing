package com.echoing.util;

public class ServiceConstants {
	/**
	 * Format of the DateTime strings
	 */
	public static final String DATETIME_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
	/**
	 * Format for the Index name date pattern
	 */
	public static final String INDEX_NAME_DATETIME_FORMAT_PATTERN = "yyyyMMdd";
	/**
	 * Index static name
	 */
	public static final String INDEX_NAME = "indexechoing";
	/**
	 * Index alias name
	 */
	public static final String INDEX_ALIAS_NAME = "echoing";
}
