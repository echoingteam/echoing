# Rest endpoints #

TODO: say something about this

##Requests

Might be useful if we want to test the endpoints I guess!

### http://<server>/echoing/rest/Message with POST

```javascript
{
	"uuid" : "123456",
	"coordinates" : {
		"latitude" : 12.0,
		"longitude" : 12.0
	},
	"messageContent" : "messageContent",
	"user" : {
		"userId" : "1",
		"email" : "test@cascomio.com"
	},
	"timestamp" : "2012-04-23 18:25:43"
}
```

### http://<server>/echoing/rest/PositionedMessages with POST

```javascript
{
	"coordinates" : {
		"latitude" : 1.0,
		"longitude" : 2.0
	},
	"user" : {
		"userId" : 1,
		"email" : "test@email.com"
	}
}
```
