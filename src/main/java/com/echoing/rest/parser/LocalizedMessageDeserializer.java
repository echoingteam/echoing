package com.echoing.rest.parser;

import java.io.IOException;

import com.echoing.model.Coordinates;
import com.echoing.model.LocalizedMessage;
import com.echoing.model.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class LocalizedMessageDeserializer extends JsonDeserializer<LocalizedMessage>{

	@Override
	public LocalizedMessage deserialize(JsonParser jp,
			DeserializationContext ctxt) throws IOException,
			JsonProcessingException {
		LocalizedMessage message = null;
		if(jp!=null){
			message = new LocalizedMessage();
			JsonNode node = jp.getCodec().readTree(jp);
			message.setUuid(node.get("uuid").asText());
			message.setMessageContent(node.get("messageContent").asText());
			message.setCoordinates(new Coordinates());
			message.getCoordinates().setLatitude(node.get("coordinates").get("lat").asDouble());
			message.getCoordinates().setLongitude(node.get("coordinates").get("lon").asDouble());
			message.setUser(new User());
			message.getUser().setEmail(node.get("user").asText());
		}
		return message;
	}

}
