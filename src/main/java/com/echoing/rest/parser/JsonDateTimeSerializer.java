package com.echoing.rest.parser;
import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import com.echoing.util.ServiceConstants;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class JsonDateTimeSerializer extends JsonSerializer<DateTime> {

		@Override
		public void serialize(DateTime time, JsonGenerator jgen,
				SerializerProvider provider) throws IOException,
				JsonProcessingException {
			jgen.writeString(DateTimeFormat.forPattern(ServiceConstants.DATETIME_FORMAT_PATTERN).print(time.getMillis()));		
		}
}
