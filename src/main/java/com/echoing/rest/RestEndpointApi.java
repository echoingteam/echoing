package com.echoing.rest;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.echoing.model.LocalizedMessage;
import com.echoing.model.UserPosition;

public interface RestEndpointApi {
	/**
	 * Stores the message in the server
	 * @param message the message to push
	 */
	ResponseEntity<String> pushMessage(LocalizedMessage message);
	/**
	 * Retrieves the messages 
	 * @param userPosition The user location
	 * @return List of messages
	 */
	List<LocalizedMessage> pullMessages(UserPosition userPosition);
}
