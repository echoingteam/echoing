package com.echoing.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.echoing.elasticsearch.service.ElasticSearchService;
import com.echoing.log.EchoingLogger;
import com.echoing.model.LocalizedMessage;
import com.echoing.model.UserPosition;

@Component
@RequestMapping("/")
public class RestEndpointApiImpl implements RestEndpointApi{

	@Autowired
	private ElasticSearchService elasticSearchService;
	
	@Autowired
	private EchoingLogger logger;
	
	@Override
	@RequestMapping(value="/Message", method = RequestMethod.POST)
	public ResponseEntity<String> pushMessage(@RequestBody(required=true) LocalizedMessage message) {
		try {
			logger.log(message);
			elasticSearchService.putLocalizedMessage(message);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error occurred while pushing message: ",e);
			return new ResponseEntity<String>(HttpStatus.I_AM_A_TEAPOT);
		}
	}


	@Override
	@RequestMapping(value="/PositionedMessages", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<LocalizedMessage> pullMessages(@RequestBody(required=true)UserPosition userPosition) {
		return elasticSearchService.pullMessages(userPosition);
	}

	/**
	 * Setter ElasticSearchService
	 * @param elasticSearchService
	 */
	public void setElasticSearchService(ElasticSearchService elasticSearchService) {
		this.elasticSearchService = elasticSearchService;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(EchoingLogger logger) {
		this.logger = logger;
	}
}
