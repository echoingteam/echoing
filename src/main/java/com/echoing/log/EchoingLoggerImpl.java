package com.echoing.log;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.echoing.model.LocalizedMessage;

@Resource
public class EchoingLoggerImpl implements EchoingLogger {

	private Logger logger = LoggerFactory.getLogger(EchoingLoggerImpl.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log(LocalizedMessage myMessage) {
		// logger.info(myMessage.toString());
		String sep = "|";
		logger.info(sep + myMessage.getMessageContent()
				+ sep + myMessage.getUuid() + sep
				+ myMessage.getTimestamp().getMillis());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error(String message, Exception e){
		logger.error(message, e);
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}
}