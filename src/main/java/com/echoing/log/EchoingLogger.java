package com.echoing.log;

import com.echoing.model.LocalizedMessage;

public interface EchoingLogger {
	/**
	 * Log the Localized Message
	 * 
	 */
	void log(LocalizedMessage myMessage);
	
	/**
	 * Log for errors
	 * @param message
	 * @param e
	 */
	void error(String message, Exception e);
}