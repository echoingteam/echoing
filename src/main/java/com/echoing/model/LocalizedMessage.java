package com.echoing.model;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.echoing.rest.parser.LocalizedMessageDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonDeserialize(using = LocalizedMessageDeserializer.class)
public class LocalizedMessage {
	/**
	 * The UUID of the message
	 */
	private String uuid;
	/**
	 * Coordinates
	 */
	private Coordinates coordinates = new Coordinates();
	/**
	 * The Message Content
	 */
	private String messageContent;
	/**
	 * The owner of the message
	 */
	private User user = new User();
	/**
	 * The message timestamp
	 */
	@JsonSerialize(using = com.echoing.rest.parser.JsonDateTimeSerializer.class)
	@JsonDeserialize(using = com.echoing.rest.parser.JsonDateTimeDeserializer.class)
	private DateTime timestamp = new DateTime(DateTimeZone.UTC);
	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}
	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	/**
	 * @return the coordinates
	 */
	public Coordinates getCoordinates() {
		return coordinates;
	}
	/**
	 * @param coordinates the coordinates to set
	 */
	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}
	/**
	 * @return the messageContent
	 */
	public String getMessageContent() {
		return messageContent;
	}
	/**
	 * @param messageContent the messageContent to set
	 */
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}
	
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * @return the timestamp
	 */
	public DateTime getTimestamp() {
		return timestamp;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(DateTime timestamp) {
		this.timestamp = timestamp;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LocalizedMessage [uuid=" + uuid + ", coordinates="
				+ coordinates + ", messageContent=" + messageContent
				+ ", user=" + user + ", timestamp=" + timestamp + "]";
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocalizedMessage other = (LocalizedMessage) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
}
