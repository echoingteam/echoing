package com.echoing.elasticsearch.util;

public enum ElasticSearchEntityType {
	/**
	 * User Type
	 */
	USER("user"),
	/**
	 * Localized Message
	 */
	LOCALIZED_MESSAGE("localizedMessage");
	/**
	 * ElasticSearch entity type
	 */
	private String type;
	/**
	 * Constructor
	 * @param esType the elastic search type
	 */
	private ElasticSearchEntityType(String esType) {
		type = esType;
	}
	/**
	 * Getter type
	 * @return the type
	 */
	public String getType(){
		return type;
	}
}
