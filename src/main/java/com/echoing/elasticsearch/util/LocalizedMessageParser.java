package com.echoing.elasticsearch.util;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;
import java.util.Map;

import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHitField;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.echoing.model.Coordinates;
import com.echoing.model.LocalizedMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Component
@Qualifier("localizedMessageParser")
public class LocalizedMessageParser implements EntityParser<LocalizedMessage>{

	private final static ObjectMapper _objectMapper = new ObjectMapper();
	
	@Override
	public XContentBuilder parse(LocalizedMessage message) throws IOException {
		return jsonBuilder().startObject()
				.field("uuid", message.getUuid())
				.field("user", message.getUser().getEmail())
				.field("messageContent", message.getMessageContent())
				.field("timestamp", message.getTimestamp().getMillis())
				.field("coordinates")
					.startObject()
						.field("lat", message.getCoordinates().getLatitude())
						.field("lon", message.getCoordinates().getLongitude())
					.endObject()
		.endObject();
	}
	
	@Override
	public XContentBuilder getMapping() throws IOException {
		return jsonBuilder().startObject()
					.startObject("localizedMessage")
						.startObject(ElasticSearchProperties.ID.getType())
							.field(ElasticSearchProperties.PATH.getType(), "uuid")
						.endObject()
						.startObject(ElasticSearchProperties.PARENT.getType())
							.field(ElasticSearchProperties.PATH.getType(), "user")
							.field(ElasticSearchProperties.TYPE.getType(), "user")
						.endObject()
						.startObject(ElasticSearchProperties.PROPERTIES.getType())
							.startObject("uuid")
								.field(ElasticSearchProperties.TYPE.getType(), 
										ElasticSearchFieldTypes.TYPE_STRING.getType())
							.endObject()
							.startObject("user")
								.field(ElasticSearchProperties.TYPE.getType(), 
										ElasticSearchFieldTypes.TYPE_STRING.getType())
							.endObject()
							.startObject("messageContent")
								.field(ElasticSearchProperties.TYPE.getType(), 
										ElasticSearchFieldTypes.TYPE_STRING.getType())
							.endObject()
							.startObject("coordinates")
                        	.field(ElasticSearchProperties.TYPE.getType(),
                        			ElasticSearchFieldTypes.TYPE_GEO_POINT.getType())
	                        .endObject()
							.startObject("timestamp")
	                    	.field(ElasticSearchProperties.TYPE.getType(),
	                    			ElasticSearchFieldTypes.TYPE_LONG.getType())
                			.endObject()
						.endObject()
						.startObject(ElasticSearchProperties.ROUTING.getType())
							.field(ElasticSearchProperties.PATH.getType(),"uuid")
						.endObject()
					.endObject()
				.endObject();
	}

	@Override
	public LocalizedMessage getEntityFromElasticSearchHit(SearchHit searchHit) {
		LocalizedMessage message = null;
		if(searchHit.getType().equals(ElasticSearchEntityType.LOCALIZED_MESSAGE.getType())){
			try {
				return _objectMapper.readValue(searchHit.source(), LocalizedMessage.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return message;
	}
}
