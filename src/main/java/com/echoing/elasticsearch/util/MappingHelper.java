package com.echoing.elasticsearch.util;

import java.io.IOException;

import org.elasticsearch.common.xcontent.XContentBuilder;

public interface MappingHelper {
	/**
	 * Returns the settings for the ES index
	 * @return XContentBuilder with the ES settings
	 * @throws IOException
	 */
	XContentBuilder getIndexSettings() throws IOException;
	/**
	 * Get the current index alias
	 * @return the index name
	 */
	String getIndexAlias();
}
