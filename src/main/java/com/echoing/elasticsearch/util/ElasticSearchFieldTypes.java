package com.echoing.elasticsearch.util;

public enum ElasticSearchFieldTypes {
	/** String type */
	TYPE_STRING("string"),
	/** Integer type */
	TYPE_INTEGER("integer"),
	/** boolean type */
	TYPE_BOOLEAN("boolean"),
	/** float type*/
	TYPE_FLOAT("float"),
	/** Geo point type */
	TYPE_GEO_POINT("geo_point"),
	/** Nested type */
	TYPE_NESTED("nested"),
	/** short type */
	TYPE_SHORT("short"),
	/** long type */
	TYPE_LONG("long");
	/**
	 * The type name
	 */
	private String type;
	/**
	 * Constructor
	 * @param type Elastic search type name
	 */
	ElasticSearchFieldTypes(String type){
		this.type = type;
	}
	/**
	 * Getter type
	 * @return the ES type
	 */
	public String getType(){
		return type;
	}
}
