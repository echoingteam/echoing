package com.echoing.elasticsearch.util;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;

import javax.annotation.Resource;

import org.elasticsearch.common.xcontent.XContentBuilder;

@Resource
public class MappingHelperImpl implements MappingHelper{
	/**
	 * The alias to search for
	 */
	private String indexAlias;
	
	/**
	 * Copied from a class in my work, no idea what does half of the properties here does
	 */
	@Override
	public XContentBuilder getIndexSettings() throws IOException{
        return jsonBuilder()
	        .startObject()
	            .startObject("analysis")
	                .startObject("analyzer")
	                    .startObject("autocomplete")
	                        .field("type", "custom")
	                        .field("tokenizer", "standard")
	                        .field("filter", new String[]{"standard", "lowercase", "stop", "ngram"})
	                    .endObject()
	                .endObject()
	                .startObject("filter")
	                    .startObject("ngram")
	                        .field("type", "edgeNGram")
	                        .field("min_gram", 3)
	                        .field("max_gram", 20)
	                    .endObject()
	                .endObject()
	            .endObject()
	            .startObject("aliases")
	            	.field(indexAlias).startObject().endObject()
	            .endObject()
	        .endObject();
	}

	/**
	 * @return the indexAlias
	 */
	public String getIndexAlias() {
		return indexAlias;
	}

	/**
	 * @param indexAlias the indexAlias to set
	 */
	public void setIndexAlias(String indexAlias) {
		this.indexAlias = indexAlias;
	}

}
