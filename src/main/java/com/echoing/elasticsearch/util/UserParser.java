package com.echoing.elasticsearch.util;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;
import java.util.Map;

import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHitField;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.echoing.model.User;

@Component
@Qualifier("userParser")
public class UserParser implements EntityParser<User>{

	/* (non-Javadoc)
	 * @see com.echoing.rest.parser.EntityParser#parse(java.lang.Object)
	 */
	@Override
	public XContentBuilder parse(User user) throws IOException {
		return jsonBuilder().startObject()
				.field("email", user.getEmail())
		.endObject();
	}


	@Override
	public XContentBuilder getMapping() throws IOException {
		return jsonBuilder().startObject()
				.startObject("user")
					.startObject(ElasticSearchProperties.ID.getType())
						.field(ElasticSearchProperties.PATH.getType(), "email")
					.endObject()
					.startObject(ElasticSearchProperties.PROPERTIES.getType())
						.startObject("email")
							.field(ElasticSearchProperties.TYPE.getType(), 
									ElasticSearchFieldTypes.TYPE_STRING.getType())
							.field(ElasticSearchProperties.INDEX.getType(), 
									ElasticSearchProperties.NOT_ANALYZED.getType())
						.endObject()
					.endObject()
				.endObject()
			.endObject();
	}


	@Override
	public User getEntityFromElasticSearchHit(SearchHit searchHit) {
		User user = null;
		if(searchHit.getType().equals(ElasticSearchEntityType.USER.getType())){
			Map<String, SearchHitField> fieldsMap = searchHit.fields();
			user = new User();
			user.setEmail(fieldsMap.get("email").getValue().toString());
			user.setUserId(searchHit.getId());
		}
		return user;
	}
	
}
