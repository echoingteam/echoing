package com.echoing.elasticsearch.util;

import java.io.IOException;

import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.search.SearchHit;

public interface EntityParser<T> {
	
	/**
	 * Returns a content builder with the object parsed
	 * @param entity the entity to parse
	 * @return the content builder
	 * @throws IOException 
	 */
	XContentBuilder parse(T entity) throws IOException;
	/**
	 * Returns the mapping for the object
	 * @return the mapping
	 * @throws IOException
	 */
	XContentBuilder getMapping() throws IOException;
	/**
	 * Parses the 
	 * @param searchHit
	 * @return
	 */
	T getEntityFromElasticSearchHit(SearchHit searchHit);
}
