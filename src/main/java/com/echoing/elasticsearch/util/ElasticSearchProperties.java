package com.echoing.elasticsearch.util;

public enum ElasticSearchProperties {
	/** type */
	TYPE("type"),
	/** properties */
	PROPERTIES("properties"),
	/** ALIAS */
	ALIAS("alias"),
	/** path */
	PATH("path"),
	/** index */
	INDEX("index"),
	/** Not analyzed field */
	NOT_ANALYZED("not_analyzed"),
	/** id */
	ID("_id"),
	/** parent */
	PARENT("_parent"),
	/** routing */
	ROUTING("_routing"),
	/** Source */
	SOURCE("_source");
	/**
	 * The type name
	 */
	private String type;
	/**
	 * Constructor
	 * @param type Elastic search type name
	 */
	ElasticSearchProperties(String type){
		this.type = type;
	}
	/**
	 * Getter type
	 * @return the ES type
	 */
	public String getType(){
		return type;
	}
}
