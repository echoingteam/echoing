package com.echoing.elasticsearch.service;

import java.io.IOException;
import java.util.List;

import org.elasticsearch.ElasticsearchException;

import com.echoing.model.LocalizedMessage;
import com.echoing.model.UserPosition;

public interface ElasticSearchService {
	/**
	 * Creates a new Elastic search index
	 */
	void createIndex() throws ElasticsearchException, IOException;
	/**
	 * Stores the localized Message in ES
	 * @param localizedMessage the localized message to store
	 * @throws IOException 
	 * @throws ElasticsearchException 
	 */
	void putLocalizedMessage(LocalizedMessage localizedMessage) throws ElasticsearchException, IOException;

	/**
	 * Gets the Localized Messages for the User position
	 * @param userPosition The user position to check
	 * @return	the Messages
	 */
	List<LocalizedMessage> pullMessages(UserPosition userPosition);
}
