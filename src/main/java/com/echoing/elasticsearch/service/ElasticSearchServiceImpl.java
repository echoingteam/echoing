package com.echoing.elasticsearch.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.geo.GeoDistance;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.search.SearchHit;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.echoing.elasticsearch.util.ElasticSearchEntityType;
import com.echoing.elasticsearch.util.EntityParser;
import com.echoing.elasticsearch.util.MappingHelper;
import com.echoing.model.LocalizedMessage;
import com.echoing.model.User;
import com.echoing.model.UserPosition;
import com.echoing.util.ServiceConstants;

@Service
public class ElasticSearchServiceImpl implements ElasticSearchService {
	/**
	 * Mapping helper for the index settings
	 */
	@Autowired
	private MappingHelper mappingHelper;
	/**
	 * The elasticsearch client
	 */
	@Autowired
	private Client elasticsearchClient;
	/**
	 * The radio in kilometers for the search
	 */
	private Double searchRadio;
	/**
	 * The max time in hours to search messages
	 */
	private Integer searchMaxTime;
	
	@Autowired
	@Qualifier("localizedMessageParser")
	private EntityParser<LocalizedMessage> localizedMessageParser;
	
	@Autowired
	@Qualifier("userParser")
	private EntityParser<User> userParser;
	/**
	 * The Simple Date formatter for the Index Name
	 */
	public final static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(ServiceConstants.INDEX_NAME_DATETIME_FORMAT_PATTERN);

	@Override
	@PostConstruct
	public void createIndex() throws ElasticsearchException, IOException {
		String indexName = getCurrentIndexName();
		boolean isIndexExists = elasticsearchClient.admin().indices().prepareExists(indexName).execute().actionGet().isExists();
		if(!isIndexExists){
			elasticsearchClient.admin().indices().create(new CreateIndexRequest(indexName)).actionGet();		
			elasticsearchClient.admin().indices().preparePutMapping(indexName).setType(ElasticSearchEntityType.USER.getType()).setSource(userParser.getMapping()).execute().actionGet();
			elasticsearchClient.admin().indices().preparePutMapping(indexName).setType(ElasticSearchEntityType.LOCALIZED_MESSAGE.getType()).setSource(localizedMessageParser.getMapping()).execute().actionGet();
			elasticsearchClient.admin().indices().prepareAliases().addAlias(indexName, mappingHelper.getIndexAlias()).execute().actionGet();
		}
	}

	@Override
	public void putLocalizedMessage(LocalizedMessage localizedMessage) throws ElasticsearchException, IOException {
		elasticsearchClient.prepareIndex(getCurrentIndexName(), ElasticSearchEntityType.LOCALIZED_MESSAGE.getType(), localizedMessage.getUuid())
		.setSource(localizedMessageParser.parse(localizedMessage)).execute().actionGet();
	}
	
	@Override
	public List<LocalizedMessage> pullMessages(UserPosition userPosition) {
		List<LocalizedMessage> localizedMessageList= new ArrayList<LocalizedMessage>();
		SearchResponse response = elasticsearchClient.prepareSearch(mappingHelper.getIndexAlias()).setTypes(ElasticSearchEntityType.LOCALIZED_MESSAGE.getType())
			.setPostFilter(FilterBuilders.andFilter(
				FilterBuilders.geoDistanceFilter("coordinates").distance(searchRadio, DistanceUnit.KILOMETERS)
					.lat(userPosition.getCoordinates().getLatitude())
					.lon(userPosition.getCoordinates().getLongitude())
					.geoDistance(GeoDistance.ARC)
			 ,FilterBuilders.rangeFilter("timestamp").gt(new DateTime(DateTimeZone.UTC).minusHours(searchMaxTime).getMillis())
				)).execute().actionGet();
		if(response.getHits().getTotalHits()>0){
			for (SearchHit searchHit : response.getHits().getHits()) {
				localizedMessageList.add(localizedMessageParser.getEntityFromElasticSearchHit(searchHit));
			}
		}
		return localizedMessageList; 
	}
	/**
	 * Returns the current index name
	 * @return the index name
	 */
	private String getCurrentIndexName(){
		return ServiceConstants.INDEX_NAME.concat(DATE_FORMATTER.format(new Date()));
	}
	
	/**
	 * @param mappingHelper the mappingHelper to set
	 */
	public void setMappingHelper(MappingHelper mappingHelper) {
		this.mappingHelper = mappingHelper;
	}
	/**
	 * @param elasticsearchClient the elasticsearchClient to set
	 */
	public void setElasticsearchClient(Client elasticsearchClient) {
		this.elasticsearchClient = elasticsearchClient;
	}
	/**
	 * @param localizedMessageParser the localizedMessageParser to set
	 */
	public void setLocalizedMessageParser(
			EntityParser<LocalizedMessage> localizedMessageParser) {
		this.localizedMessageParser = localizedMessageParser;
	}

	/**
	 * @param searchRadio the searchRadio to set
	 */
	public void setSearchRadio(Double searchRadio) {
		this.searchRadio = searchRadio;
	}

	/**
	 * @param searchMaxTime the searchMaxTime to set
	 */
	public void setSearchMaxTime(Integer searchMaxTime) {
		this.searchMaxTime = searchMaxTime;
	}

	/**
	 * @param userParser the userParser to set
	 */
	public void setUserParser(EntityParser<User> userParser) {
		this.userParser = userParser;
	}
}
