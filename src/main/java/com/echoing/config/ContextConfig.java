package com.echoing.config;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.echoing.elasticsearch.service.ElasticSearchService;
import com.echoing.elasticsearch.service.ElasticSearchServiceImpl;
import com.echoing.elasticsearch.util.EntityParser;
import com.echoing.elasticsearch.util.LocalizedMessageParser;
import com.echoing.elasticsearch.util.MappingHelper;
import com.echoing.elasticsearch.util.MappingHelperImpl;
import com.echoing.elasticsearch.util.UserParser;
import com.echoing.log.EchoingLogger;
import com.echoing.log.EchoingLoggerImpl;
import com.echoing.model.LocalizedMessage;
import com.echoing.model.User;

@Configuration
@PropertySource("classpath:appProperties.properties")
public class ContextConfig {

	@Autowired
	private Environment env;
	
	@Bean
	public EchoingLogger getEchoingLogger(){
		return new EchoingLoggerImpl();
	}
	
	@Bean
	public Client getClient() {
		Settings settings = ImmutableSettings.settingsBuilder()
				.put("cluster.name", env.getProperty("cluster.name")).build();
		return new TransportClient(settings)
        .addTransportAddress(new InetSocketTransportAddress(env.getProperty("host1"), 9300));
	}
	
	@Bean
	public ElasticSearchService getElasticSearchService(){
		ElasticSearchServiceImpl elasticSearchService = new ElasticSearchServiceImpl();
		elasticSearchService.setElasticsearchClient(getClient());
		elasticSearchService.setMappingHelper(getMappingHelper());
		elasticSearchService.setSearchMaxTime(Integer.parseInt(env.getProperty("index.search.time")));
		elasticSearchService.setSearchRadio(Double.parseDouble(env.getProperty("index.search.radio")));
		return elasticSearchService;
	}
	
	@Bean
	public MappingHelper getMappingHelper(){
		MappingHelperImpl mappingHelper =  new MappingHelperImpl();
		mappingHelper.setIndexAlias(env.getProperty("index.alias.read"));
		return mappingHelper;
	}
	
	@Bean
	@Qualifier("localizedMessageParser")
	public EntityParser<LocalizedMessage> getLocalizedMessageParser(){
		return new LocalizedMessageParser();
	}
	
	@Bean
	@Qualifier("userParser")
	public EntityParser<User> getUserParser(){
		return new UserParser();
	}
	
}
